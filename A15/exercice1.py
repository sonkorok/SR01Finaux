from tkinter import *
from tkinter import messagebox

def ajouterContact(event):
    nom = entreeNom.get()
    numero = entreeNumero.get()
    listContacts.insert(END, nom + " : " + numero)

def supprimerContact(event):
    try:
        listContacts.delete(listContacts.curselection())
    except Exception:
        messagebox.showinfo("Message", "Numéro non trouvé")
    else:
        messagebox.showinfo("Message", "Numéro supprimé avec succès")

def valider(event):
    return 0

fenetre = Tk()
fenetre.title("Contacts")

cadre1 = Frame(fenetre, bd = 6, bg = "#80c0c0")
labelNom = Label(cadre1, text = "Nom", bg = "#80c0c0")
labelNumero = Label(cadre1, text = "Numero", bg = "#80c0c0")
entreeNom = Entry(cadre1, relief = GROOVE)
entreeNumero = Entry(cadre1, relief = GROOVE)
buttonAjContact = Button(cadre1, text = "Ajouter un contact")
buttonAjContact.bind("<1>", ajouterContact)

cadre1.pack()
labelNom.grid(row = 1, padx = 10, pady = 5)
labelNumero.grid(row = 2, padx = 10, pady = 5)
entreeNom.grid(row = 1, column = 2, padx = 10, pady = 5)
entreeNumero.grid(row = 2, column = 2, padx = 10, pady = 5)
buttonAjContact.grid(row = 1, column = 3, padx = 10, pady = 5)

cadre2 = Frame(fenetre, bd = 2)
labelRepertoire = Label(cadre2, text = "Mon repertoire:")
listContacts = Listbox(cadre2, height = 4)
buttonSupContact = Button(cadre2, text = "Supprimer")
buttonSupContact.bind("<1>", supprimerContact)

cadre2.pack()
labelRepertoire.pack(side = TOP, padx = 10, pady = 5)
listContacts.pack(side = LEFT, padx = 10, pady = 5)
buttonSupContact.pack(side = RIGHT, padx = 10, pady = 5)

fenetre.mainloop()
