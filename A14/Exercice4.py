def initialiser_matrice(N=10):
    matrice = [[0 for i in range(0, N)] for j in range(0, N)]
    for i in range(0, N):
        for j in range(0, N):
            matrice[i][j] = int(input("ligne " + str(i) + ", colonne " + str(j) + ": "))
    return matrice

def matrice_creuse(matrice, N=10):
    n = 0
    for i in range(0, N):
        for j in range(0, N):
            if matrice[i][j] == 0:
                n = n + 1
    if n / (N * N) > 0.8:
        return True
    else:
        return False

n = int(input("Taille de la matrice: "))
matrice=initialiser_matrice(n)

if (matrice_creuse(matrice,n)) :
    print("La matrice est creuse")
else:
    print("La matrice n'est pas creuse")
