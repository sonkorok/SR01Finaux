
/* =========================================================
 * Questions 1 & 2 & 3
 * ========================================================= */

// fopen: http://www.tutorialspoint.com/c_standard_library/c_function_fopen.htm
// fwrite: https://www.tutorialspoint.com/c_standard_library/c_function_fwrite.htm
// fread: http://www.tutorialspoint.com/c_standard_library/c_function_fread.htm

void ecrire_fils(int nb, char* name)
{
  FILE *f = fopen(name, "w+");
  fwrite(&nb, sizeof(int), 1, f); // on écrit 1 élément de taille sizeof(int)
  fclose(f);
}

void lire_pere(int* nb, char* name)
{
  char *cmd = malloc(sizeof(char) * 100);

  // Lecture
  FILE *f = fopen(name, "r");
  fread(nb, sizeof(int), 1, f); // on lit 1 élément de taille sizeof(int)
  fclose(f);

  // Destruction
  strcpy(cmd, "rm ");
  strcat(cmd, name);
  system(cmd);
}

/* =========================================================
 * Question 4
 * ========================================================= */

int pid1, pid2, status, nb1, nb2;
char* name1 = "fic1.txt";
char* name2 = "fic2.txt";

void handler_fils1(int x){
  waitpid(pid2, &status, 0); // on attend le 2ème fils
  ecrire_fils(nb1, name1);   // nb1 et name1 sont des variables globales
}
void handler_fils2(int x){
  ecrire_fils(nb2, name2);  // nb2 et name2 sont des variables globales
}

int main() {

  pid1 = fork();

  if(pid1 ==0)
  {
    // On est dans le 1er processus fils
    nb1 = 1000;
    signal(SIGUSR1, handler_fils1);
    kill(getpid(), SIGUSR1);
    return 0;
  }
  else
  {
    pid2 = fork();

    if(pid2 == 0)
    {
      // On est dans le 2ème processus fils
      nb2 = 556;
      signal(SIGUSR1, handler_fils2);
      kill(getpid(), SIGUSR1);
      return 0;
    }
    else
    {
      // On est dans le processus père
      int j1 = 0, j2 = 0;
      sleep(1);
      waitpid(pid1, &status, 0); // on attend le 1er fils (qui lui même a attendu le 2ème fils)
      lire_pere(&j1, name1);
      lire_pere(&j2, name2);
      return 0;
    }

  }

}
