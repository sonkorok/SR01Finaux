kill(getpid(),SIGINT); a pour effet de provoquer un arrêt brutal du processus actuel, qui ne déclenchera pas
l'exécution de la fonction passée à atexit().

Lors du exit(1), par contre, le processus est terminé normalment donc la fonction passée en atexit() sera
exécutée.

Conclusion:
- Si x == 1, le processus se termine brutalement => pas de atexit()
- Sinon, le processus se termine normalement => atexit() 
